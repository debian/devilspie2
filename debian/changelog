devilspie2 (0.43-5) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 14:51:08 +0100

devilspie2 (0.43-4) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Update standards version to 4.5.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 04 Feb 2021 02:05:00 +0000

devilspie2 (0.43-3) unstable; urgency=medium

  [ Andreas Rönnquist ]
  * QA upload
  * Fix debian-watch-uses-insecure-uri
  * Use minimal gpg key, fixes public-upstream-key-not-minimal
  * Use Standards-Version 4.3.0 (No changes required)

  [ Jelmer Vernooĳ ]
  * Use secure copyright file specification URI.
  * Trim trailing whitespace.

  [ Helmut Grohne ]
  * Fix FTCBFS: (Closes: #912289)
    + Let dh_auto_build pass cross tools to make.
    + 0001-cross.patch-Make-pkg-config-substitutable: Make pkg-cofig
	  substitutable.

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 26 Jan 2019 18:33:06 +0100

devilspie2 (0.43-2) unstable; urgency=medium

  * QA upload.
  * Update Vcs-*-fields to salsa

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 21 Jul 2018 13:24:37 +0200

devilspie2 (0.43-1) unstable; urgency=medium

  * QA upload.
  * New upstream version 0.43
  * Update homepage field
  * Update to Standards Version 4.1.0 (No changes required)
  * Set QA Group as maintainer

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 26 Sep 2017 16:09:17 +0200

devilspie2 (0.42-2) unstable; urgency=medium

  * Fix watch file
  * Update Standards Version, no changes required
  * Update to compat level 10

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 03 Jul 2017 21:33:51 +0200

devilspie2 (0.42-1) unstable; urgency=medium

  * New upstream version 0.42
  * Update Standards-Version to 3.9.8 (No changes required)

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 14 Jan 2017 13:56:06 +0100

devilspie2 (0.41-1) unstable; urgency=medium

  * New Upstream Version

 -- Andreas Rönnquist <gusnan@debian.org>  Thu, 07 Apr 2016 18:01:51 +0200

devilspie2 (0.40-1) unstable; urgency=medium

  * Imported Upstream version 0.40
  * Update copyright years

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 29 Mar 2016 20:32:56 +0200

devilspie2 (0.39-5) unstable; urgency=medium

  * Add hardening flags
  * Fix lintian warning vcs-field-uses-insecure-uri
  * Update Standards Version, no changes required

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 06 Mar 2016 10:48:51 +0100

devilspie2 (0.39-4) unstable; urgency=medium

  * Fix installation of translations

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 26 Dec 2015 19:00:37 +0100

devilspie2 (0.39-3) unstable; urgency=medium

  * Fix usage of PREFIX during build

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 26 Dec 2015 15:28:09 +0100

devilspie2 (0.39-2) unstable; urgency=medium

  * Migrate to dh

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 26 Dec 2015 13:37:08 +0100

devilspie2 (0.39-1) unstable; urgency=medium

  * Imported Upstream version 0.39
  * Update copyright, fixing dep5-copyright-license-name-not-unique

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 11 May 2015 17:06:21 +0200

devilspie2 (0.38-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.6 (No changes required)
  * Fix spelling errors in package description, thanks
    to Reuben Thomas (Closes: #783248)

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 26 Apr 2015 19:52:15 +0200

devilspie2 (0.38-1) unstable; urgency=medium

  * Update watch file with noredirect URL
  * Imported Upstream version 0.38

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 30 Sep 2014 20:59:53 +0200

devilspie2 (0.37-1) unstable; urgency=medium

  * Imported Upstream version 0.37
  * Migrate to my @d.o address

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 22 Aug 2014 13:01:57 +0200

devilspie2 (0.36-2) unstable; urgency=medium

  * Update watchfile to use savannah
  * Add upstream gpg key and make uscan check it

 -- Andreas Rönnquist <gusnan@gusnan.se>  Sat, 28 Jun 2014 13:45:12 +0200

devilspie2 (0.36-1) unstable; urgency=medium

  * New Upstream Version

 -- Andreas Rönnquist <gusnan@gusnan.se>  Wed, 21 May 2014 15:03:48 +0200

devilspie2 (0.35-1) unstable; urgency=medium

  * New Upstream Version

 -- Andreas Rönnquist <gusnan@gusnan.se>  Mon, 19 May 2014 03:37:53 +0200

devilspie2 (0.33-1) unstable; urgency=medium

  * New Upstream Version

 -- Andreas Rönnquist <gusnan@gusnan.se>  Fri, 04 Apr 2014 15:48:31 +0200

devilspie2 (0.32-1) unstable; urgency=medium

  * Imported Upstream version 0.32
  * Update to Standards-Version 3.9.5

 -- Andreas Rönnquist <gusnan@gusnan.se>  Thu, 20 Feb 2014 15:43:44 +0100

devilspie2 (0.31-1) unstable; urgency=low

  * Imported Upstream version 0.31

 -- Andreas Rönnquist <gusnan@gusnan.se>  Thu, 29 Aug 2013 17:40:30 +0200

devilspie2 (0.30-1) unstable; urgency=low

  * Imported Upstream version 0.30

 -- Andreas Rönnquist <gusnan@gusnan.se>  Sat, 20 Jul 2013 12:08:00 +0200

devilspie2 (0.29-1) unstable; urgency=low

  * Imported Upstream version 0.29

 -- Andreas Rönnquist <gusnan@gusnan.se>  Sat, 18 May 2013 21:24:17 +0200

devilspie2 (0.28-3) unstable; urgency=low

  * Upload to unstable
  * [5e0d0ea] Update to standards Version 3.9.4 (No change required)

 -- Andreas Rönnquist <gusnan@gusnan.se>  Sun, 05 May 2013 13:56:20 +0200

devilspie2 (0.28-2) experimental; urgency=low

  * [79c06d4] Migrate to debhelper

 -- Andreas Rönnquist <gusnan@gusnan.se>  Sun, 17 Mar 2013 22:10:35 +0100

devilspie2 (0.28-1) experimental; urgency=low

  * [9d80eb6] Imported Upstream version 0.28

 -- Andreas Rönnquist <gusnan@gusnan.se>  Tue, 19 Feb 2013 00:04:18 +0100

devilspie2 (0.27-1) experimental; urgency=low

  * [d7717df] Imported Upstream version 0.27

 -- Andreas Rönnquist <gusnan@gusnan.se>  Sun, 16 Dec 2012 21:15:53 +0100

devilspie2 (0.26-1) experimental; urgency=low

  * [1164344] Imported Upstream version 0.26
  * [565c496] Remove patch fix_set_window_position2.patch, applied upstream

 -- Andreas Rönnquist <gusnan@gusnan.se>  Wed, 14 Nov 2012 17:29:19 +0100

devilspie2 (0.25-2) experimental; urgency=low

  * [0594635] Minor update to description
  * [691a113] Add patch fix_set_window_position2.patch

 -- Andreas Rönnquist <gusnan@gusnan.se>  Sun, 04 Nov 2012 18:34:24 +0100

devilspie2 (0.25-1) experimental; urgency=low

  * [c34c2a6] Imported Upstream version 0.25

 -- Andreas Rönnquist <gusnan@gusnan.se>  Sun, 28 Oct 2012 17:39:01 +0100

devilspie2 (0.24-1) experimental; urgency=low

  * [5a4c2fb] Imported Upstream version 0.24

 -- Andreas Rönnquist <gusnan@gusnan.se>  Tue, 16 Oct 2012 14:25:52 +0200

devilspie2 (0.23-1) experimental; urgency=low

  * [ec07697] Imported Upstream version 0.23

 -- Andreas Rönnquist <gusnan@gusnan.se>  Sat, 29 Sep 2012 10:28:21 +0200

devilspie2 (0.22-1) experimental; urgency=low

  * [6db8920] Imported Upstream version 0.22

 -- Andreas Rönnquist <gusnan@gusnan.se>  Sun, 23 Sep 2012 16:54:45 +0200

devilspie2 (0.20-1) unstable; urgency=low

  * [46eb2ec] Imported Upstream version 0.20

 -- Andreas Rönnquist <gusnan@gusnan.se>  Sat, 23 Jun 2012 20:42:44 +0200

devilspie2 (0.19-2) unstable; urgency=low

  * [c137f99] Add INSTALL_PREFIX=/usr to DEB_MAKE_BUILD_TARGET

 -- Andreas Rönnquist <gusnan@gusnan.se>  Tue, 19 Jun 2012 20:32:36 +0200

devilspie2 (0.19-1) unstable; urgency=low

  * [0cd1a60] Imported Upstream version 0.19

 -- Andreas Rönnquist <gusnan@gusnan.se>  Mon, 23 Apr 2012 16:01:24 +0200

devilspie2 (0.18-1) unstable; urgency=low

  * [aac593d] Add Vcs-* fields
  * [07f4c1d] Update copyright to version 1.0
  * [849fd50] Update standards version to 3.9.3
  * [8e7159e] Imported Upstream version 0.18

 -- Andreas Rönnquist <gusnan@gusnan.se>  Tue, 17 Apr 2012 20:04:08 +0200

devilspie2 (0.17-1) unstable; urgency=low

  * New upstream release

 -- Andreas Rönnquist <gusnan@gusnan.se>  Wed, 11 Jan 2012 16:10:21 +0100

devilspie2 (0.15-1) unstable; urgency=low

  * New upstream release

 -- Andreas Rönnquist <gusnan@gusnan.se>  Sat, 19 Nov 2011 13:44:42 +0100

devilspie2 (0.14-1) unstable; urgency=low

  * Initial release (Closes: #644416)

 -- Andreas Rönnquist <gusnan@gusnan.se>  Mon, 14 Nov 2011 02:19:32 +0200
